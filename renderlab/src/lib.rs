use wasm_bindgen::prelude::*;

use std::{
    future::Future,
    sync::Arc,
    task::{Context, Poll, Wake, Waker},
    panic::PanicInfo,
};

pub fn busy_wait<T>(mut future: impl Future<Output = T>) -> T {
    struct IdleWaker;
    impl Wake for IdleWaker {
        fn wake(self: Arc<Self>) {}
    }

    let mut pinned_fut = unsafe { std::pin::Pin::new_unchecked(&mut future) };
    let waker = Waker::from(Arc::new(IdleWaker {}));
    let mut context = Context::from_waker(&waker);
    loop {
        match pinned_fut.as_mut().poll(&mut context) {
            Poll::Pending => (),
            Poll::Ready(result) => {
                return result;
            }
        }
    }
}

struct Renderer {
    device: wgpu::Device,
    surface: wgpu::Surface,
    queue: wgpu::Queue,
    instance: wgpu::Instance,
    surface_format: wgpu::TextureFormat,
    pipeline: wgpu::RenderPipeline,
}

impl Renderer {
    async fn new(window: &winit::window::Window) -> Self {
        let instance = wgpu::Instance::default();

        let surface = unsafe { instance.create_surface(&window) }.unwrap();

        let adapter_future = instance.request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            compatible_surface: Some(&surface),
            force_fallback_adapter: false,
        });

        let adapter = busy_wait(adapter_future).unwrap();

        let (device, queue) = busy_wait(adapter.request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: adapter.features(),
                limits: adapter.limits(),
            },
            None,
        ))
        .unwrap();

        let surface_format = *surface.get_capabilities(&adapter).formats.first().unwrap();
        surface.configure(
            &device,
            &wgpu::SurfaceConfiguration {
                usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
                format: surface_format,
                width: window.inner_size().width,
                height: window.inner_size().height,
                present_mode: wgpu::PresentMode::Fifo,
                alpha_mode: wgpu::CompositeAlphaMode::Auto,
                view_formats: [surface_format].to_vec(),
            },
        );

        let code = std::borrow::Cow::Borrowed(include_str!("default_shader.wgsl"));
        let vertex_shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("Default vertex shader"),
            source: wgpu::ShaderSource::Wgsl(code.clone()),
        });
        let fragment_shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("Default fragment shader"),
            source: wgpu::ShaderSource::Wgsl(code),
        });

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: None,
            vertex: wgpu::VertexState {
                module: &vertex_shader,
                entry_point: "vertex_main",
                buffers: &[],
            },
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Cw,
                cull_mode: Some(wgpu::Face::Back),
                unclipped_depth: false,
                polygon_mode: wgpu::PolygonMode::Fill,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState::default(),
            fragment: Some(wgpu::FragmentState {
                module: &fragment_shader,
                entry_point: "fragment_main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: surface_format,
                    blend: None,
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }),
            multiview: None,
        });

        Self {
            device,
            queue,
            instance,
            surface,
            surface_format,
            pipeline,
        }
    }

    fn render(&mut self) {
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

        let surface_texture = self.surface.get_current_texture().unwrap();
        let surface_view = surface_texture
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &surface_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color::BLUE),
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.pipeline);
            render_pass.draw(0..3, 0..1);
        }

        let buffer = encoder.finish();

        self.queue.submit([buffer]);

        surface_texture.present();
    }
}

#[wasm_bindgen]
pub fn main() {
    let event_loop = winit::event_loop::EventLoop::new();
    let mut builder = winit::window::WindowBuilder::new();
    builder = builder.with_title("Renderlab");
    let window = builder.build(&event_loop).unwrap();

    #[cfg(target_arch = "wasm32")]
    {
        use winit::platform::web::WindowExtWebSys;

        console_log::init_with_level(log::Level::Debug).expect("could not initialize logger");

        std::panic::set_hook(Box::new(|info: &PanicInfo| {
            let msg = format!("{}", info);
            web_sys::console::error_1(&msg.into());
        }));

        web_sys::window()
            .and_then(|win| win.document())
            .and_then(|doc| doc.body())
            .and_then(|body| {
                body.append_child(&web_sys::Element::from(window.canvas()))
                    .ok()
            })
            .expect("couldn't append canvas to document body");
    };

    let mut renderer = busy_wait(Renderer::new(&window));

    event_loop.run(move |event, _, control_flow| match event {
        winit::event::Event::RedrawRequested(_) => {
            renderer.render();
            window.request_redraw();
        }
        winit::event::Event::WindowEvent { event, .. } => match event {
            winit::event::WindowEvent::CloseRequested => {
                *control_flow = winit::event_loop::ControlFlow::Exit;
            }
            _ => (),
        },
        _ => (),
    });
}
