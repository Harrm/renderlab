use std::io::Read;

use actix_web::{get, http::header::ContentType, web, App, HttpResponse, HttpServer, Responder, middleware::Logger};
use env_logger::Env;

#[get("/hello/{name}")]
async fn greet(name: web::Path<String>) -> impl Responder {
    format!("Hello {name}!")
}

#[get("/pkg/renderlab.js")]
async fn package() -> impl Responder {
    let mut content = String::new();
    std::fs::File::open("renderlab/pkg/renderlab.js")
        .unwrap()
        .read_to_string(&mut content)
        .unwrap();

    HttpResponse::Ok()
        .content_type("text/javascript")
        .body(content)
}

#[get("/")]
async fn index() -> impl Responder {
    let mut content = String::new();
    std::fs::File::open("html/index.html")
        .unwrap()
        .read_to_string(&mut content)
        .unwrap();

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(content)
}

#[get("/pkg/renderlab_bg.wasm")]
async fn wasm() -> impl Responder {
    let mut content: Vec<u8> = Vec::new();
    std::fs::File::open("renderlab/pkg/renderlab_bg.wasm")
        .unwrap()
        .read_to_end(&mut content)
        .unwrap();

    HttpResponse::Ok()
        .content_type("application/wasm")
        .body(content)
}

#[actix_web::main] // or #[tokio::main]
async fn main() -> std::io::Result<()> {

    env_logger::init_from_env(Env::default().default_filter_or("info"));

    HttpServer::new(|| {
        App::new()
            .wrap(Logger::default())
            .service(greet)
            .service(index)
            .service(wasm)
            .service(package)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
